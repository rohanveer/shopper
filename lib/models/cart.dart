import 'package:velocity_x/velocity_x.dart';

import 'package:shopper/core/store.dart';
import 'package:shopper/models/catalog.dart';

class CartModel {
  //catalog field
  late CatatlogModel _catatlogModel;
  //collection of ids - store IDs of each item
  final List<int> _itemIds = [];
  //get catalog
  CatatlogModel get catalog => _catatlogModel;
  //
  set catalog(CatatlogModel newCatalog) {
    _catatlogModel = newCatalog;
  }

  //get items in the cart
  List<Items> get items =>
      _itemIds.map((id) => _catatlogModel.getById(id)).toList();

  num get totalPrice =>
      items.fold(0, (total, current) => total + current.price);
}

class AddMutation extends VxMutation<MyStore> {
  final Items item;
  AddMutation(this.item);

  @override
  perform() {
    store!.cart._itemIds.add(item.id);
  }
}

class RemoveMutation extends VxMutation<MyStore> {
  final Items item;
  RemoveMutation(this.item);

  @override
  perform() {
    store!.cart._itemIds.remove(item.id);
  }
}
