// ignore_for_file: null_closures

import 'dart:convert';

class Items {
  late final int id;
  late final String name;
  late final String desc;
  late final num price;
  late final String color;
  late final String image;

  Items({
    required this.id,
    required this.name,
    required this.desc,
    required this.price,
    required this.color,
    required this.image,
  });

  Items copyWith({
    int? id,
    String? name,
    String? desc,
    num? price,
    String? color,
    String? image,
  }) {
    return Items(
      id: id ?? this.id,
      name: name ?? this.name,
      desc: desc ?? this.desc,
      price: price ?? this.price,
      color: color ?? this.color,
      image: image ?? this.image,
    );
  }

  Map<String, dynamic> toMap() {
    final result = <String, dynamic>{};

    result.addAll({'id': id});
    result.addAll({'name': name});
    result.addAll({'desc': desc});
    result.addAll({'price': price});
    result.addAll({'color': color});
    result.addAll({'image': image});

    return result;
  }

  factory Items.fromMap(Map<String, dynamic> map) {
    return Items(
      id: (map['id']),
      name: (map['name']),
      desc: (map['desc']),
      price: (map['price']),
      color: (map['color']),
      image: (map['image']),
    );
  }

  String toJson() => json.encode(toMap());

  factory Items.fromJson(String source) => Items.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Items(id: $id, name: $name, desc: $desc, price: $price, color: $color, image: $image)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Items &&
        other.id == id &&
        other.name == name &&
        other.desc == desc &&
        other.price == price &&
        other.color == color &&
        other.image == image;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        name.hashCode ^
        desc.hashCode ^
        price.hashCode ^
        color.hashCode ^
        image.hashCode;
  }
}

class CatatlogModel {
  static List<Items> items = [];

  //get item by id

   Items getById(int id) =>
      items.firstWhere((element) => element.id == id, orElse: null);

  // get item by position
   Items getByPosition(int pos) => items[pos];
}
/*
> first bracket in json data file is called json object
> "products": [ next is object key and it's value is stored in [] squareBrackets in array/list form
>  {
            "id": 1,
            "name": "iPhone 12 Pro",
            "desc": "Apple iPhone 12th generation",
            "price": 999,
            "color": "#33505a",
            "image": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRISJ6msIu4AU9_M9ZnJVQVFmfuhfyJjEtbUm3ZK11_8IV9TV25-1uM5wHjiFNwKy99w0mR5Hk&usqp=CAc"
        },

        > this is another json object which contains key and value
        
  // factory Items.fromMap(Map<String, dynamic> map) {
  //   return Items(
  //     id: map["id"],
  //     name: map["name"],
  //     desc: map["desc"],
  //     price: map["price"],
  //     color: map["color"],
  //     image: map["image"],
  //   );
  // }

  // toMap() => {
  //       "id": id,
  //       "name": name,
  //       "desc": desc,
  //       "price": price,
  //       "color": color,
  //       "image": image,
  //     };

*/
