import 'package:shopper/models/cart.dart';
import 'package:shopper/models/catalog.dart';
import 'package:velocity_x/velocity_x.dart';

class MyStore extends VxStore {
  late CatatlogModel catalogModel;
  late CartModel cart;

  MyStore() {
    catalogModel = CatatlogModel();
    cart = CartModel();
    cart.catalog = catalogModel;
  }
}
