import 'package:flutter/material.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    const url =
        "https://firebasestorage.googleapis.com/v0/b/twitch-ea2ce.appspot.com/o/rohan--min.jpg?alt=media&token=22eddfd4-cccf-4133-93f2-91b288f5ff79";
    return Drawer(
      child: Container(
        color: Colors.lightGreen,
        child: ListView(
          padding: EdgeInsets.zero,
          children: const [
            DrawerHeader(
              padding: EdgeInsets.zero,
              child: UserAccountsDrawerHeader(
                accountName: Text("Rohan Veer"),
                accountEmail: Text("rohanveer04@gmail.com"),
                margin: EdgeInsets.zero,
                currentAccountPicture: CircleAvatar(
                  backgroundImage: NetworkImage(url),
                ),
              ),
            ),
            ListTile(
              leading: Icon(Icons.home, color: Colors.black),
              title: Text("home", textScaleFactor: 1.2),
            ),
            ListTile(
              leading: Icon(Icons.person, color: Colors.black),
              title: Text("profile", textScaleFactor: 1.2),
            ),
            ListTile(
              leading: Icon(Icons.settings, color: Colors.black),
              title: Text("settings", textScaleFactor: 1.2),
            ),
            ListTile(
              leading: Icon(Icons.share, color: Colors.black),
              title: Text("share", textScaleFactor: 1.2),
            ),
          ],
        ),
      ),
    );
  }
}
