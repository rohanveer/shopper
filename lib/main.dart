import 'package:flutter/material.dart';
import 'package:shopper/core/store.dart';
import 'package:shopper/pages/cart_page.dart';
import 'package:shopper/pages/login_page.dart';
import 'package:shopper/utils/routes.dart';
import 'package:shopper/widgets.dart/themes.dart';
import 'package:velocity_x/velocity_x.dart';
import 'pages/home_page.dart';

void main() {
  runApp(VxState(
    store: MyStore(),
    child: Shopper(),
  ));
}

class Shopper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      themeMode: ThemeMode.system,
      theme: AppTheme.lightTheme(context),
      darkTheme: AppTheme.darkTheme(context),
      debugShowCheckedModeBanner: false,
      initialRoute: AppRoutes.homeRoute,
      routes: {
        AppRoutes.homeRoute: (context) => HomePage(),
        AppRoutes.loginRoute: (context) => const LoginPage(),
        AppRoutes.cartRoute: (context) => const CartPage(),
      },
    );
  }
}
